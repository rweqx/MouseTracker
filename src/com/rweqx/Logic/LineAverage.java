package com.rweqx.Logic;

import java.util.ArrayList;

public class LineAverage {
    public void LineAverage(){

    }


    //Uncertainty
    double uy = 1; //pixels
    double uy2 = 1;

    public double[] calculateAverageLine(ArrayList<int[]> clicks) {
        double OneoverUY2 = 0;
        double XYoverUY2 = 0;
        double XoverUY2 = 0;
        double YoverUY2 = 0;
        double X2overUY2 = 0;
        int count = clicks.size();

        for (int[] i : clicks) {
            double x = i[0];
            double y = i[1];

            if (y == 0) {
                y = 0.01;
            }
            OneoverUY2 += 1.0 / uy2;
            XYoverUY2 += x * y / uy2;
            XoverUY2 += x / uy2;
            YoverUY2 += y / uy2;
            X2overUY2 += x * x / uy2;
        }

        //  System.out.println("OneOverUY2 " + OneoverUY2 + " XOverUY2 " + XoverUY2 + " Count  " + count)
        //  System.out.println(XYoverUY2 + " " + X2overUY2);
        double slope = 0;
        double yint = 0;
        if (count > 1) {
            double delta = OneoverUY2 * X2overUY2 - Math.pow(XoverUY2, 2);
            slope = 1.0 / delta * (OneoverUY2 * XYoverUY2 - XoverUY2 * YoverUY2);
            yint = 1.0 / delta * (X2overUY2 * YoverUY2 - XoverUY2 * XYoverUY2);

            System.out.println("Approximation of slope and y int: " +
                    "\nDelta: " + delta +
                    "\nSlope: " + slope +
                    "\nyint :" + yint
            );

        }
        double d[] = {slope, yint};

        return d;


    }
}
