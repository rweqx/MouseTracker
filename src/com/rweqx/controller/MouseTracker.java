package com.rweqx.controller;

import com.rweqx.view.Window;
import org.jnativehook.GlobalScreen;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseListener;
import org.jnativehook.mouse.NativeMouseMotionListener;

import java.math.BigDecimal;
import java.util.ArrayList;

public class MouseTracker implements NativeMouseListener, NativeMouseMotionListener {

    public MouseTracker(){
        lastLoc[0] = -1;
        lastLoc[1] = -1;

    }

    public void startTracking(){
        //binding Native Hook
        GlobalScreen.getInstance().addNativeMouseListener(this);
        GlobalScreen.getInstance().addNativeMouseMotionListener(this);

    }

    int iLClicks = 0;
    int iRClicks = 0;
    int iClicks = 0;
    double iDistanceMoved = 0;


    ArrayList<int[]> clicks = new ArrayList<int[]>();
    ArrayList<int[]> movement = new ArrayList<int[]>();
    int[] lastLoc = new int[2];

    @Override
    public void nativeMouseClicked(NativeMouseEvent nativeMouseEvent) {}

    final double pixelsToInches = Math.sqrt(Math.pow(1920, 2) + Math.pow(1080, 2))/24.0;
    final double inchesToCm = 2.54;

    @Override
    public void nativeMouseMoved(NativeMouseEvent nativeMouseEvent) {
        int[] i = new int[2];
        i[0] = nativeMouseEvent.getX();
        i[1] = nativeMouseEvent.getY();
        if(!(lastLoc[0] == i[0] && lastLoc[1] == i[1])){
            double distance = Math.sqrt(Math.pow((i[0] - lastLoc[0]), 2) + Math.pow((i[1] - lastLoc[1]), 2));
            iDistanceMoved += distance/pixelsToInches*inchesToCm/100.0;

            movement.add(i);
            lastLoc[0] = i[0];
            lastLoc[1] = i[1];
        }
    }

    @Override
    public void nativeMousePressed(NativeMouseEvent nativeMouseEvent) {
        int[] i = new int[2];
        i[0] = nativeMouseEvent.getX();
        i[1] = nativeMouseEvent.getY();
        System.out.println("Click");
        if(nativeMouseEvent.getButton() == 1){
            System.out.println("L Click");
            iLClicks++;
        }else if(nativeMouseEvent.getButton() == 2){
            iRClicks ++;
        }
        iClicks ++;


        clicks.add(i);
    }

    @Override
    public void nativeMouseReleased(NativeMouseEvent nativeMouseEvent) {}


    @Override
    public void nativeMouseDragged(NativeMouseEvent nativeMouseEvent) {
        int[] i = new int[2];
        i[0] = nativeMouseEvent.getX();
        i[1] = nativeMouseEvent.getY();
        if(!(lastLoc[0] == i[0] && lastLoc[1] == i[1])){
            double distance = Math.sqrt(Math.pow((i[0] - lastLoc[0]), 2) + Math.pow((i[1] - lastLoc[1]), 2));
            iDistanceMoved += distance/pixelsToInches*inchesToCm/100.0;

            movement.add(i);
            lastLoc[0] = i[0];
            lastLoc[1] = i[1];
        }
    }

    public int getiLClicks() {
        return iLClicks;
    }

    public int getiRClicks() {
        return iRClicks;
    }

    public int getiClicks() {
        return iClicks;
    }

    public double getiDistance() {
        return iDistanceMoved;
    }

    public ArrayList<int[]> getClickLocs(){
        return clicks;
    }
    public ArrayList<int[]> getMovementLocs(){
        return movement;
    }

    public void reset() {
        clicks = new ArrayList<int[]>();
        movement = new ArrayList<int[]>();
        iLClicks = 0;
        iRClicks = 0;
        iClicks = 0;
        iDistanceMoved = 0;
    }
}
