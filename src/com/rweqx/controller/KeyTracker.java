package com.rweqx.controller;

import org.jnativehook.GlobalScreen;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.awt.*;
import java.util.ArrayList;

public class KeyTracker implements NativeKeyListener {
    public void KeyTracker(){

    }

    public void startTracking(){
        GlobalScreen.getInstance().addNativeKeyListener(this);
    }

    ArrayList<Object[]> keyPress = new ArrayList<>();

    @Override
    public void nativeKeyPressed(NativeKeyEvent event) {
        Object s[] = new Object[2];
        s[0] = NativeKeyEvent.getKeyText(event.getKeyCode());
        System.out.println(s[0]);

        Point p = MouseInfo.getPointerInfo().getLocation();
        s[1] = p;

        keyPress.add(s);

    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {

    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {

    }
}
