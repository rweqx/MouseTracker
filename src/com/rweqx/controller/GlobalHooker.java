package com.rweqx.controller;

import org.jnativehook.GlobalScreen;

public class GlobalHooker {
    public GlobalHooker() {
        try {
            System.out.println("binding hook");
            GlobalScreen.registerNativeHook();

            System.out.println("Hook bound " + GlobalScreen.isNativeHookRegistered());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
