package com.rweqx.view;

import com.sun.javafx.scene.paint.GradientUtils;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class DrawLine {
    Stage window;

    public void drawLines(double[] movement, double[] clicks) {
        window = new Stage();
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        //set Stage boundaries to visible bounds of the main screen
        window.setX(primaryScreenBounds.getMinX());
        window.setY(primaryScreenBounds.getMinY());
        window.setWidth(primaryScreenBounds.getWidth());
        window.setHeight(primaryScreenBounds.getHeight());

        StackPane SP = new StackPane();

        Canvas canvas = new Canvas(1920, 1080);

        System.out.println("Movement in Black, Clicks in Blue");
        drawLine(movement[0], movement[1], canvas.getGraphicsContext2D(), Color.BLACK);
        drawLine(clicks[0], clicks[1], canvas.getGraphicsContext2D(), Color.BLUE);
        SP.setStyle("-fx-background-color: DAE6F3;");
        SP.getChildren().add(canvas);




        window.setScene(new Scene(SP, 1920, 1080));
        window.initModality(Modality.APPLICATION_MODAL);
        window.show();
    }

    private void drawLine(double slope, double yint, GraphicsContext gc, Color c){
        //y = mx+b

        boolean leftWall = false;
        boolean rightWall = false;

        if(yint >= 0 && yint <= 1080){
            leftWall = true;
        }

        double exitY= 1920*slope+yint;
        if(exitY <= 1080 && exitY >= 0){
            rightWall = true;
        }

        
        Point p1, p2;
        if(leftWall){
            //Left Wall (0, yint)
            p1 = new Point(0, yint);
        }else{
            if(slope >= 0){
                //TopWall ( X, 0);
                p1 = new Point(-1.0*yint/slope, 0);
            }else{
                //Bottom Wall (X, 1080)
                p1 = new Point((1080-yint)/slope, 1080);
            }
        }
        if(rightWall){
            //Right Wall (1920, exitY);
            p2 = new Point(1920, exitY);
        }else{
            if(slope>=0) {
                //Bottom Wall (X, 1080);
                p2 = new Point((1080 - yint) / slope, 1080);
            }else {
                //Top Wall (X, 0);
                p2 = new Point(-1.0*yint/slope, 0);
            }
        }

        gc.setFill(c);
        gc.setStroke(c);
        gc.setLineWidth(1);
        gc.strokeLine(p1.x, p1.y, p2.x, p2.y);
    }

    public class Point {
        public double x;
        public double y;

        public Point(double x, double y){
            this.x = x;
            this.y = y;
        }
    }
}
