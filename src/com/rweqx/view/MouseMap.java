package com.rweqx.view;

import com.rweqx.controller.MouseTracker;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;


public class MouseMap {
    MouseTracker MT;
    double Vfactor = 2.0/3.0;
    double Hfactor = 2.0/3.0;

    double screenH, screenW;



    long w = Math.round(1920.0 * Hfactor);
    long h = Math.round(1080.0 * Vfactor);

    public MouseMap(MouseTracker MT) {
        this.MT = MT;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        screenW = screenSize.getWidth();
        screenH = screenSize.getHeight();
    }

    Stage window;
    Scene scene;
    Canvas canvas;

    public void showMouseMap(int width, int height) {
        if(width != -1 && height != -1) {
            w = width;
            h = height;
            Hfactor = width / screenW;
            Vfactor = height / screenH;
            System.out.println("Creating Mouse Map with following Specs" +
                    "\nWidth " + width +
                    "\nHeight " + height +
                    "\nVFac " + Vfactor +
                    "\nHFac " + Hfactor);

        }

        window = new Stage();
        VBox root = new VBox(10);
        root.setAlignment(Pos.CENTER);

        HBox buttons = new HBox(10);
        buttons.setAlignment(Pos.CENTER);
        Button button = new Button("Close");
        Button saveButton = new Button("Save");
        buttons.getChildren().addAll(button, saveButton);

        button.setOnAction(e-> {
            window.close();
        });

        saveButton.setOnAction(e->{
            saveToImage();
        });

        canvas = new Canvas(w, h);
        //GraphicsContext gc = canvas.getGraphicsContext2D();
        //gc.setFill(Color.rgb(199, 202, 237));
        //gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        StackPane canvasHolder = new StackPane();
        canvasHolder.setStyle("-fx-background-color: DAE6F3;");
        canvasHolder.getChildren().add(canvas);




        drawLines(canvas.getGraphicsContext2D());
        drawDots(canvas.getGraphicsContext2D());


        root.getChildren().addAll(canvasHolder, buttons);

        scene = new Scene(root, w, h+50);
        //SNEAKY, BUT WE'RE GONNA MAKE THE HEIGHT ACTUALLY 50 LARGER THAN THE SELECTED SIZE,
        // BECAUSE THAT LOOKS BETTER (KEEPS THE ASPECT RATIO)


        window.setScene(scene);
        window.initModality(Modality.APPLICATION_MODAL);
        window.show();

    }

    private void drawLines(GraphicsContext gc) {
        gc.setFill(Color.BLACK);
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(1);

        ArrayList<int[]> moves = MT.getMovementLocs();

        long lastX = -1;
        long lastY = -1;

        for(int i=0; i < moves.size(); i++){
            long x = Math.round(((double)moves.get(i)[0]) * Hfactor);
            long y = Math.round(((double)moves.get(i)[1]) * Vfactor);

            if(lastX != -1) {
                gc.strokeLine(lastX, lastY, x, y);
            }
            lastX = x;
            lastY = y;
        }
    }
    //Radius of oval
    final int r = 5;

    private void drawDots(GraphicsContext gc){
        gc.setFill(Color.BLUE);
        gc.setStroke(Color.BLUE);
        gc.setLineWidth(1);

        ArrayList<int[]> clicks = MT.getClickLocs();
        for(int i=0; i<clicks.size(); i++){

            int x = (int)Math.round(((double)clicks.get(i)[0]) * Hfactor - (r/2));
            int y = (int)Math.round(((double)clicks.get(i)[1]) * Vfactor - (r/2));

            gc.fillOval(x, y, r, r);
        }

    }

    private void saveToImage(){
        System.out.println("Saving Image to File");

        WritableImage WI = canvas.snapshot(new SnapshotParameters(),
                new WritableImage((int)Math.round(w), (int)Math.round(h)));
        File file = new File("Drawing.png");

        try{
            ImageIO.write(SwingFXUtils.fromFXImage(WI, null), "png", file);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
