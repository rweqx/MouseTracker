package com.rweqx.view;

import com.rweqx.Logic.LineAverage;
import com.rweqx.controller.GlobalHooker;
import com.rweqx.controller.KeyTracker;
import com.rweqx.controller.MouseTracker;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.jnativehook.GlobalScreen;

import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;

public class Window extends Application {

    Text lClicks, lDistance;

    MouseTracker MT;

    public static void main(String args[]) {
        Platform.setImplicitExit(true);
        launch(args);
    }

    MouseMap MM;
    Button button;

    TextField fHeight, fWidth;
    Label lHeight, lWidth;

    KeyTracker KT;

    @Override
    public void start(Stage primaryStage) throws Exception {
        GlobalHooker GH = new GlobalHooker();

        MT = new MouseTracker();
        MM = new MouseMap(MT);
        KT = new KeyTracker();

        MT.startTracking();
        KT.startTracking();

        primaryStage.setTitle("Easy Mouse Tracker");

        lClicks = new Text();
        lDistance = new Text();


        button = new Button("Show Mouse Map");
        button.setOnAction(e -> {
            int x = -1;
            int y = -1;
            try {
                x = Integer.valueOf(fWidth.getText());
                y = Integer.valueOf(fHeight.getText());
            } catch (Exception ex) {
                //ex.printStackTrace();
                //Ignroe the error.
            }
            MM.showMouseMap(x, y);
        });

        Button avgButton = new Button("Calculate Click Avg Line");
        avgButton.setOnAction(e->{
            LineAverage LA = new LineAverage();

            System.out.println("Average Axis for clicks");
            double d[] = LA.calculateAverageLine(MT.getClickLocs());
            System.out.println("Average Axis for movement");
            double d2[] = LA.calculateAverageLine(MT.getMovementLocs());

            DrawLine DL = new DrawLine();
            DL.drawLines(d2, d);

        });


        Button resetButton = new Button("Reset");
        resetButton.setOnAction(e -> {
            MT.reset();
            Platform.runLater(() -> {
                fHeight.setText("720");
                fWidth.setText("1280");
            });

        });


        //    HBox labels = new HBox(10);
        //    labels.setAlignment(Pos.CENTER);
        //    labels.getChildren().addAll(lClicks, lDistance);

        fHeight = new TextField("720");
        fWidth = new TextField("1280");
        fHeight.setPrefColumnCount(10);
        fWidth.setPrefColumnCount(10);
        lHeight = new Label("Height:");
        lWidth = new Label("Width:");

        GridPane fields = new GridPane();
        fields.setPadding(new Insets(10, 10, 10, 10));
        fields.setHgap(10);
        fields.setVgap(10);
        fields.setAlignment(Pos.CENTER);

        fields.add(lWidth, 0, 0);
        fields.add(fWidth, 1, 0);
        fields.add(lHeight, 0, 1);
        fields.add(fHeight, 1, 1);
        //fields.setStyle("-fx-background-color: #081060");


        VBox box = new VBox(10);
        box.setAlignment(Pos.CENTER);

        box.getChildren().addAll(lClicks, lDistance, fields, button, resetButton, avgButton);


        primaryStage.setScene(new Scene(box, 800, 400));
        primaryStage.show();

        TimerTask timerTask = new MyTimerTask();
        //running timer task as daemon thread
        timer = new Timer(true);
        timer.scheduleAtFixedRate(timerTask, 0, 100);

    }

    Timer timer;

    public class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            updateClicks();
            updateDistance();
        }
    }


    public void updateClicks() {
        int iLClicks = MT.getiLClicks();
        int iRClicks = MT.getiRClicks();
        int iClicks = MT.getiClicks();
        Platform.runLater(() -> {
            lClicks.setText("Clicks (L R Total) " + iLClicks + " " + iRClicks + " " + iClicks);
        });
    }

    public void updateDistance() {
        double iDistance = MT.getiDistance();
        DecimalFormat DF = new DecimalFormat("#.00");
        Platform.runLater(() -> {
            lDistance.setText("Distance " + DF.format(iDistance));
        });
    }

    @Override
    public void stop() {
        GlobalScreen.unregisterNativeHook();
        timer.cancel();
    }
}
