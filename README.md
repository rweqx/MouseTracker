Basic Mouse Tracker for use in Games. 

Goals:
*   Analyze key/mouse presses
*   Analyze mouse movement
*   Generate heatmaps and visual representation of information.


Coded in Java in IntelliJ
